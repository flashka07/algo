(
 (c-mode . ((eval . (add-project-directories
					 "include"
					 "src"
					 "test"
					 "test/googletest/googletest/include"
					 ))
			(eval . (add-project-compiler-flags
					 "-std=c++11"
					 ))
			))
 (c++-mode . ((eval . (add-project-directories
					   "include"
					   "src"
					   "test"
					   "test/googletest/googletest/include"
					   ))
			  (eval . (add-project-compiler-flags
					   "-std=c++11"
					   ))
			  ))
 )
