#ifndef __LOG_H_
#define __LOG_H_

#include <iostream>

class LogStreamWrapper
{
public:
    explicit LogStreamWrapper(std::ostream& stream);
    ~LogStreamWrapper();
    std::ostream& stream() const;
private:
    std::ostream& m_stream;
};

template<class T>
std::ostream& operator<<(const LogStreamWrapper& wrapper, T&& value)
{
    std::ostream& stream = wrapper.stream();
    stream << value;
    return stream;
}

enum LogLevel : uint8_t
{
    Debug,
    Info,
    Warning,
    Error
};

LogStreamWrapper LogMessage(LogLevel level);

#define LOG(level) LogMessage(LogLevel::level)
#define LOGD LOG(Debug)
#define LOGI LOG(Info)
#define LOGW LOG(Warning)
#define LOGE LOG(Error)

void installSignalHandlers();

#endif /* __LOG_H_ */
