#include "utils/timer.h"

#include <sstream>

Timer::Timer()
    : m_isValid(false)
{
}

bool Timer::isValid() const
{
    return m_isValid;
}

void Timer::invalidate()
{
    m_isValid = false;
}

void Timer::start()
{
    if (!isValid())
        restart();
}

void Timer::restart()
{
    m_isValid = true;
    m_startPoint = Clock::now();
}

Timer::Duration Timer::elapsed() const
{
    if (isValid())
        return Clock::now() - m_startPoint;
    return Duration();
}

std::chrono::nanoseconds Timer::elapsedNs() const
{
    using namespace std::chrono;
    return duration_cast<nanoseconds>(elapsed());
}

std::chrono::microseconds Timer::elapsedUs() const
{
    using namespace std::chrono;
    return duration_cast<microseconds>(elapsed());
}

std::chrono::milliseconds Timer::elapsedMs() const
{
    using namespace std::chrono;
    return duration_cast<milliseconds>(elapsed());
}

std::chrono::seconds Timer::elapsedS() const
{
    using namespace std::chrono;
    return duration_cast<seconds>(elapsed());
}

std::string Timer::toString(const std::string& action) const
{
    static const std::string INVALID("timer is not valid");
    if (!isValid())
        return INVALID;
    return elapsedToString(elapsed(), action);
}

std::string Timer::elapsedToString(const Duration& interval, const std::string& action)
{
    using namespace std::chrono;
    int value = 0;
    const char* unit = "";
    auto timeRaw = interval;
    seconds timeS = duration_cast<seconds>(timeRaw);
    if (timeS.count() > 9) {
        value = timeS.count();
        unit = "s";
    }
    if (!value) {
        milliseconds timeMs = duration_cast<milliseconds>(timeRaw);
        if (timeMs.count() > 9) {
            value = timeMs.count();
            unit = "ms";
        }
    }
    if (!value) {
        microseconds timeUs = duration_cast<microseconds>(timeRaw);
        if (timeUs.count() > 9) {
            value = timeUs.count();
            unit = "us";
        }
    }
    if (!value) {
        nanoseconds timeNs = duration_cast<nanoseconds>(timeRaw);
        if (timeNs.count() > 9) {
            value = timeNs.count();
            unit = "ns";
        }
    }
    std::stringstream buf;
    if (!action.empty())
        buf << "action " << '\"' << action << "\" done in ";
    buf << value << ' ' << unit;
    if (!action.empty())
        buf << '\n';

    return buf.str();
}
