#ifndef __TIMER_H_
#define __TIMER_H_

#include <chrono>
#include <string>

class Timer
{
public:
    using Clock = std::chrono::high_resolution_clock;
    using TimePoint= Clock::time_point;
    using Duration = Clock::duration;

    Timer();
    bool isValid() const;
    void invalidate();
    void start();
    void restart();

    Duration elapsed() const;
    std::chrono::nanoseconds elapsedNs() const;
    std::chrono::microseconds elapsedUs() const;
    std::chrono::milliseconds elapsedMs() const;
    std::chrono::seconds elapsedS() const;

    std::string toString(const std::string& action = std::string()) const;

    static std::string elapsedToString(const Duration& interval,
                                       const std::string& action = std::string());

private:
    bool m_isValid;
    TimePoint m_startPoint;
};

#endif /* __TIMER_H_ */
