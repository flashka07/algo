#include "utils/log.h"

#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>
#include <unistd.h>
#include <cxxabi.h>

#include <vector>

typedef struct _sig_ucontext {
    unsigned long     uc_flags;
    struct ucontext   *uc_link;
    stack_t           uc_stack;
    struct sigcontext uc_mcontext;
    sigset_t          uc_sigmask;
} sig_ucontext_t;

void criticalErrorHandler(int sig_num, siginfo_t * info, void * ucontext);

LogStreamWrapper::LogStreamWrapper(std::ostream& stream)
    : m_stream(stream)
{}

LogStreamWrapper::~LogStreamWrapper()
{
    m_stream << '\n';
}

std::ostream& LogStreamWrapper::stream() const
{
    return m_stream;
}

LogStreamWrapper LogMessage(LogLevel level)
{
    std::ostream& stream = std::cout;
    switch(level) {
    case LogLevel::Debug:
        stream << "D ";
        break;
    case LogLevel::Info:
        stream << "I ";
        break;
    case LogLevel::Warning:
        stream << "W ";
        break;
    case LogLevel::Error:
        stream << "E ";
    }
    return LogStreamWrapper(stream);
}

void installSignalHandlers()
{
    // signal(SIGSEGV, criticalErrorHandler);

    struct sigaction sigact {0};

    sigact.sa_sigaction = criticalErrorHandler;
    sigact.sa_flags = SA_RESTART | SA_SIGINFO;

    std::vector<int> signals { SIGFPE, SIGSEGV, SIGBUS, SIGSYS, SIGILL, SIGHUP, SIGABRT };
    for (int i : signals) {
        if (sigaction(i, &sigact, (struct sigaction *)NULL) != 0) {
            std::cerr << "error setting signal handler for"
                      << i << '(' << static_cast<const char*>(strsignal(i)) << ")\n";
            exit(EXIT_FAILURE);
        }
    }
}

void criticalErrorHandler(int sig_num, siginfo_t* info, void* ucontext)
{
    sig_ucontext_t * uc = (sig_ucontext_t *)ucontext;

#if defined(__i386__) // gcc specific
    void* caller_address = (void *) uc->uc_mcontext.eip; // EIP: x86 specific
#elif defined(__x86_64__) // gcc specific
    void* caller_address = (void *) uc->uc_mcontext.rip; // RIP: x86_64 specific
#else
#error Unsupported architecture. // TODO: Add support for other arch.
#endif

    std::cerr << "signal " << sig_num
              << " (" << static_cast<const char*>(strsignal(sig_num)) << "), address is "
              << info->si_addr << " from " << caller_address
              << "\n\n";

    void * array[50];
    int size = backtrace(array, 50);

    array[1] = caller_address;

    char ** messages = backtrace_symbols(array, size);
    // skip first stack frame (points here)
    for (int i = 1; i < size && messages != NULL; ++i) {
        char *mangled_name = 0, *offset_begin = 0, *offset_end = 0;

        // find parantheses and +address offset surrounding mangled name
        for (char *p = messages[i]; *p; ++p) {
            if (*p == '(') {
                mangled_name = p;
            } else if (*p == '+') {
                offset_begin = p;
            } else if (*p == ')') {
                offset_end = p;
                break;
            }
        }

        // if the line could be processed, attempt to demangle the symbol
        if (mangled_name && offset_begin && offset_end &&
            mangled_name < offset_begin) {
            *mangled_name++ = '\0';
            *offset_begin++ = '\0';
            *offset_end++ = '\0';

            int status;
            char * real_name = abi::__cxa_demangle(mangled_name, 0, 0, &status);

            // if demangling is successful, output the demangled function name
            if (status == 0) {
                std::cerr << "[bt]: (" << i << ") " << messages[i] << " : "
                          << real_name << "+" << offset_begin << offset_end
                          << '\n';

            } else {
                // otherwise, output the mangled function name
                std::cerr << "[bt]: (" << i << ") " << messages[i] << " : "
                          << mangled_name << "+" << offset_begin << offset_end
                          << '\n';
            }
            free(real_name);
        } else {
            // otherwise, print the whole line
            std::cerr << "[bt]: (" << i << ") " << messages[i] << '\n';
        }
        // char syscom[256];
        // sprintf(syscom, "addr2line %p -e bin/algo", array[i]); //last parameter is the name of this app
        // system(syscom);
    }
    std::cerr << '\n';

    free(messages);

    exit(EXIT_FAILURE);
}
