#ifndef __PRINTER_H_
#define __PRINTER_H_

#include <string>
#include <iostream>

template <class Stream, class Iter>
void printData(Stream& stream, Iter first, Iter last, bool brief = false, const std::string& label = std::string())
{
    auto size = std::distance(first, last);
    if (!label.empty())
        stream << label << ' ';
    stream << '(' << size << "): [";

    if (size < 3)
        brief = false;

    if (!brief) {
        for (decltype(size) i = 0; first != last; ++i, first = std::next(first)) {
            stream << *first;
            if (i != size - 1)
                stream << ' ';
        }
    } else {
        stream << *first << " .. ";
        for (decltype(size) i = 0; i < size - 1; ++i, first = std::next(first));
        stream << *first;
    }
    stream << ']';
}

template <class Iter>
void printData(Iter first, Iter last, bool brief = false, const std::string& label = std::string())
{
    printData(std::cout, first, last, brief, label);
    std::cout << '\n';
}

template <class Iter>
class PrintSeq
{
public:
    PrintSeq(const Iter& _first, const Iter& _last, bool _brief)
        : first(_first), last(_last), brief(_brief)
    {}
    Iter first;
    Iter last;
    bool brief;
};

template <class Iter>
std::ostream& operator<<(std::ostream& stream, const PrintSeq<Iter>& seq)
{
    printData(stream, seq.first, seq.last, seq.brief);
    return stream;
}

template <class Iter>
PrintSeq<Iter> printSeq(const Iter& first, const Iter& last, bool brief = false)
{
    return PrintSeq<Iter>(first, last, brief);
}

#endif /* __PRINTER_H_ */
