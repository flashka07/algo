#include <iostream>
#include <unordered_map>
#include <exception>

#include "registras.h"
#include "data_structures_module.h"
#include "search_module.h"
#include "sort_module.h"

#include "utils/log.h"
#include "utils/timer.h"

std::unordered_map<std::string, ActionInfo> g_actions;

int listAllActions(int argc, char** argv)
{
    const std::string ACTIONS("--actions");
    for (const auto& item : g_actions) {
        if (item.first != ACTIONS) {
            std::cout << item.first << " - " << item.second.description << '\n';
        }
    }
    return 0;
}

void registerActions()
{
    g_actions["--actions"] = ActionInfo(listAllActions, "");

    registerDataStructuresModule();
    registerSearchModule();
    registerSortModule();
}

void printAdditionalUsageMessage()
{
    std::cout << "You may list all acctions with: algo --actions\n";
}

int main(int argc, char* argv[])
{
    installSignalHandlers();
    if (argc < 2) {
        std::cout << "Usage: algo <action> [...]\n";
        printAdditionalUsageMessage();
        return 1;
    }

    registerActions();

    std::string action(argv[1]);
    auto actionIter = g_actions.find(action);
    if (actionIter == g_actions.cend()) {
        std::cout << "Action \"" << action << "\" not found\n";
        printAdditionalUsageMessage();
        return 1;
    }

    Timer timer;
    timer.start();
    int result = 0;
    try {
        result = actionIter->second.func(argc - 2, argv + 2);
    } catch (std::exception& ex) {
        LOGE << "exception: " << ex.what();
        result = 1;
    } catch (...) {
        LOGE << "unknown exception";
        result = 1;
    }
    auto elapsed = timer.elapsed();
    std::cout << Timer::elapsedToString(elapsed, action);
    return result;
}

void registerAction(const std::string& action, ActionFunc func, const std::string& description)
{
    registerAction(action, ActionInfo(func, description));
}

void registerAction(const std::string& action, ActionInfo info)
{
    g_actions[action] = info;
}
