#include "data_structures_module.h"
#include "registras.h"

#include <algo/heap.h>

#include <array>

#include "utils/log.h"
#include "utils/printer.h"

int heapBasic(int argc, char** argv)
{
    using algo::heap;

    heap<int> dheap;
    LOGI << "Default constructed. Empty: " << std::boolalpha << dheap.empty();
    dheap.push(0);
    LOGI << "Push 0. Heap " << printSeq(dheap.cbegin(), dheap.cend());
    heap<int> dheap2(dheap);
    LOGI << "Copy constructed. Empty: " << std::boolalpha << dheap2.empty() << ' ' << printSeq(dheap2.cbegin(), dheap2.cend());
    heap<int> dheap3(std::move(dheap2));
    LOGI << "Move constructed. Empty: " << std::boolalpha << dheap3.empty() << ' ' << printSeq(dheap3.cbegin(), dheap3.cend());
    heap<int> dheap4 {0, 5, 10};
    LOGI << "Initializer list constructed. Size: " << dheap4.size() << ' ' << printSeq(dheap4.cbegin(), dheap4.cend());

    dheap2 = dheap3;
    LOGI << "Copy operator= constructed. Empty: " << std::boolalpha << dheap2.empty() << ' ' << printSeq(dheap2.cbegin(), dheap2.cend());

    dheap3.push(1);
    LOGI << "Push 1. Heap" << printSeq(dheap3.cbegin(), dheap3.cend());

    dheap2 = std::move(dheap3);
    LOGI << "Move operator= constructed. Empty: " << std::boolalpha << dheap2.empty() << ' ' << printSeq(dheap2.cbegin(), dheap2.cend());

    dheap4 = {1, 6, 11};
    LOGI << "Initializer list opeartor= constructed. Size: " << dheap4.size() << ' ' << printSeq(dheap4.cbegin(), dheap4.cend());

    LOGI << "1th element of heap: " << dheap4[1] << " == " << dheap4.at(1);
    LOGI << "Top element of heap: " << dheap4.top() << " == " << dheap4.front();
    LOGI << "Back element of heap: " << dheap4.back();

    dheap.assign(dheap4.cbegin(), dheap4.cend());
    LOGI << "Iterator assign constructed. Size: " << dheap.size() << ' ' << printSeq(dheap.cbegin(), dheap.cend());

    dheap4.assign({2, 7, 12, 17});
    LOGI << "Initializer list assign constructed. Size: " << dheap4.size() << ' ' << printSeq(dheap4.cbegin(), dheap4.cend());

    dheap4.pop();
    LOGI << "Popped element " << printSeq(dheap4.cbegin(), dheap4.cend());

    int oldValue = dheap4[1];
    dheap4.set(dheap4.begin() + 1, 22);
    LOGI << "Set 1th element from " << oldValue << " to 22 " << printSeq(dheap4.cbegin(), dheap4.cend());

    dheap4.erase(dheap4.begin() + 1);
    LOGI << "Erased 1th element " << printSeq(dheap4.cbegin(), dheap4.cend());

    dheap.swap(dheap4);
    LOGI << "Swapped " << printSeq(dheap.cbegin(), dheap.cend()) << " and " << printSeq(dheap4.cbegin(), dheap4.cend());

    dheap2.clear();
    LOGI << "Cleared heap. Empty: " << std::boolalpha << dheap2.empty() << ' ' << printSeq(dheap2.cbegin(), dheap2.cend());

    auto buf = dheap.sorted();
    LOGI << "Sorted sequence " << printSeq(buf.cbegin(), buf.cend());

    LOGI << printSeq(dheap.cbegin(), dheap.cend()) << " == " << printSeq(dheap4.cbegin(), dheap4.cend()) << " is " << std::boolalpha << (dheap == dheap4);
    LOGI << printSeq(dheap.cbegin(), dheap.cend()) << " != " << printSeq(dheap4.cbegin(), dheap4.cend()) << " is " << std::boolalpha << (dheap != dheap4);
    LOGI << printSeq(dheap.cbegin(), dheap.cend()) << " < " << printSeq(dheap4.cbegin(), dheap4.cend()) << " is " << std::boolalpha << (dheap < dheap4);
    LOGI << printSeq(dheap.cbegin(), dheap.cend()) << " <= " << printSeq(dheap4.cbegin(), dheap4.cend()) << " is " << std::boolalpha << (dheap <= dheap4);
    LOGI << printSeq(dheap.cbegin(), dheap.cend()) << " > " << printSeq(dheap4.cbegin(), dheap4.cend()) << " is " << std::boolalpha << (dheap > dheap4);
    LOGI << printSeq(dheap.cbegin(), dheap.cend()) << " >= " << printSeq(dheap4.cbegin(), dheap4.cend()) << " is " << std::boolalpha << (dheap >= dheap4);

    return 0;
}

int heapAdapterBasic(int argc, char** argv)
{
    using Buffer = std::array<int, 5>;
    using algo::heap_adapter;

    Buffer buffer;
    heap_adapter<Buffer::iterator> aheap(buffer.begin(), buffer.end());

    LOGI << "Heap adapter built. Empty: " << std::boolalpha << aheap.empty();
    aheap.push(0);
    LOGI << "0 pushed. Heap " << printSeq(aheap.cbegin(), aheap.cend());
    aheap.push(1);
    LOGI << "1 pushed. Heap " << printSeq(aheap.cbegin(), aheap.cend());
    aheap.push(2);
    LOGI << "2 pushed. Heap " << printSeq(aheap.cbegin(), aheap.cend());
    aheap.push(3);
    LOGI << "3 pushed. Heap " << printSeq(aheap.cbegin(), aheap.cend());
    aheap.push(4);
    LOGI << "4 pushed. Heap " << printSeq(aheap.cbegin(), aheap.cend());

    aheap.sort();
    LOGI << "Sorted buffer contents " << printSeq(buffer.cbegin(), buffer.cend());

    aheap.build(buffer.begin(), buffer.end(), buffer.end());
    LOGI << "Heap adapter built another time " << printSeq(aheap.cbegin(), aheap.cend());

    LOGI << "Top element = " << aheap.top();
    aheap.pop();
    LOGI << "Popped element. Heap (size " << aheap.size() << " from max " << aheap.max_size() << ") "
         << printSeq(aheap.cbegin(), aheap.cend())
         << ", buffer " << printSeq(buffer.cbegin(), buffer.cend());

    aheap.erase(aheap.cbegin() + 1);
    LOGI << "Erased 1th element. Heap " << printSeq(aheap.cbegin(), aheap.cend())
         << ", buffer " << printSeq(buffer.cbegin(), buffer.cend());

    int oldValue = *(aheap.cbegin() + 1);
    aheap.set(aheap.cbegin() + 1, 10);
    LOGI << "Set 1th element from " << oldValue << " to 10 " << printSeq(aheap.cbegin(), aheap.cend())
         << ", buffer " << printSeq(buffer.cbegin(), buffer.cend());

    return 0;
}

void registerDataStructuresModule()
{
    registerAction("heap", heapBasic, "Heap data structure basic test");
    registerAction("heap_adapter", heapAdapterBasic, "Heap adapter for any data structure basic test");
}
