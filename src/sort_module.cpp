#include "sort_module.h"
#include "registras.h"

#include <algo/sort.h>
#include <algo/generator.h>

#include <vector>

#include "utils/log.h"
#include "utils/printer.h"
#include "utils/timer.h"

enum SortType : uint8_t
{
    Insertion,
    Bubble,
    Selection,
    Merge_recursive,
    Merge,
    Heap,
    Heap_std,
    Quick_recursive,
    Quick_recursive_random,
    Quick,
    Quick_random,
    Std_sort,

    Counting,
    Bucket,
};

template <class Iter, class Comp>
void heapStdSort(Iter first, Iter last, Comp comp)
{
    std::make_heap(first, last, comp);
    std::sort_heap(first, last, comp);
}

template <class Iter>
void heapStdSort(Iter first, Iter last)
{
    heapStdSort(first, last, std::less<decltype(*first)>());
}

template <class ...Args>
void sortData(SortType type, Args&&... args)
{
    switch (type) {
    case SortType::Insertion:
        algo::insertion_sort(std::forward<Args>(args)...);
        break;
    case SortType::Bubble:
        algo::bubble_sort(std::forward<Args>(args)...);
        break;
    case SortType::Selection:
        algo::selection_sort(std::forward<Args>(args)...);
        break;
    case SortType::Merge_recursive:
        algo::merge_sort_recursive(std::forward<Args>(args)...);
        break;
    case SortType::Merge:
        algo::merge_sort(std::forward<Args>(args)...);
        break;
    case SortType::Heap:
        algo::heap_sort(std::forward<Args>(args)...);
        break;
    case SortType::Heap_std:
        heapStdSort(std::forward<Args>(args)...);
        break;
    case SortType::Quick_recursive:
        algo::quick_sort_recursive(std::forward<Args>(args)...);
        break;
    case SortType::Quick_recursive_random:
        algo::quick_sort_recursive_random(std::forward<Args>(args)...);
        break;
    case SortType::Quick:
        algo::quick_sort(std::forward<Args>(args)...);
        break;
    case SortType::Quick_random:
        algo::quick_sort_random(std::forward<Args>(args)...);
        break;
    case SortType::Std_sort:
        std::sort(std::forward<Args>(args)...);
        break;
    default:
        LOGE << "unknown sort type " << type;
        break;
    }
}

template <class ...Args>
void sortDataMaxLimited(SortType type, Args&&... args)
{
    switch (type) {
    case SortType::Counting:
        algo::counting_sort_limited(std::forward<Args>(args)...);
        break;
    default:
        LOGE << "unknown sort type " << type;
        break;
    }
}

template <class ...Args>
void sortDataFloatLimited(SortType type, Args&&... args)
{
    switch (type) {
    case SortType::Bucket:
        algo::bucket_sort(std::forward<Args>(args)...);
        break;
    default:
        LOGE << "unknown sort type " << type;
        break;
    }
}

int sortBasic(int argc, char** argv, SortType sortType)
{
    bool randomFill = false;
    if (argc > 0)
        randomFill = std::string(argv[0]) == "rnd";
    int size = 10;
    if (argc > 1)
        size = std::stoi(std::string(argv[1]), nullptr);
    bool silent = false;
    if (argc > 2)
        silent = std::string(argv[2]) == "--silent";

    std::vector<int> data(size > 0 ? size : 10);
    algo::generator::generate_data(data.begin(), data.end(), randomFill);

    Timer timer;

    LOGI << "input " << printSeq(data.cbegin(), data.cend(), silent);
    timer.start();
    sortData(sortType, data.begin(), data.end());
    auto elapsed = timer.elapsed();
    LOGI << "sorted <less> " << Timer::elapsedToString(elapsed) << ' '
         << printSeq(data.cbegin(), data.cend(), silent);
    timer.restart();
    sortData(sortType, data.begin(), data.end(), std::greater<int>());
    elapsed = timer.elapsed();
    LOGI << "sorted <greater> " << Timer::elapsedToString(elapsed) << ' '
         << printSeq(data.cbegin(), data.cend(), silent);

    return 0;
}

int sortMaxLimited(int argc, char** argv, SortType sortType)
{
    bool randomFill = false;
    if (argc > 0)
        randomFill = std::string(argv[0]) == "rnd";
    int size = 10;
    if (argc > 1)
        size = std::stoi(std::string(argv[1]), nullptr);
    bool silent = false;
    if (argc > 2)
        silent = std::string(argv[2]) == "--silent";

    std::vector<uint16_t> data(size > 0 ? size : 10);
    uint16_t vmin = 0;
    uint16_t vmax = std::min<size_t>(data.size(), std::numeric_limits<uint16_t>::max());
    algo::generator::generate_data(data.begin(), data.end(), vmin, vmax, randomFill);

    Timer timer;

    LOGI << "input " << printSeq(data.cbegin(), data.cend(), silent);
    timer.start();
    sortDataMaxLimited(sortType, data.begin(), data.end(), vmax);
    auto elapsed = timer.elapsed();
    LOGI << "sorted (success is " << std::boolalpha << std::is_sorted(data.cbegin(), data.cend())
         << ") " << Timer::elapsedToString(elapsed) << ' '
         << printSeq(data.cbegin(), data.cend(), silent);

    return 0;
}

int sortFloatLimited(int argc, char** argv, SortType sortType)
{
    bool randomFill = false;
    if (argc > 0)
        randomFill = std::string(argv[0]) == "rnd";
    int size = 10;
    if (argc > 1)
        size = std::stoi(std::string(argv[1]), nullptr);
    bool silent = false;
    if (argc > 2)
        silent = std::string(argv[2]) == "--silent";

    std::vector<float> data(size > 0 ? size : 10);
    float vmin = 0;
    float vmax = 1;
    algo::generator::generate_data(data.begin(), data.end(), vmin, vmax, randomFill);

    Timer timer;

    LOGI << "input " << printSeq(data.cbegin(), data.cend(), silent);
    timer.start();
    sortDataFloatLimited(sortType, data.begin(), data.end());
    auto elapsed = timer.elapsed();
    LOGI << "sorted (success is " << std::boolalpha << std::is_sorted(data.cbegin(), data.cend())
         << ") " << Timer::elapsedToString(elapsed) << ' '
         << printSeq(data.cbegin(), data.cend(), silent);

    return 0;
}

int insertionSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Insertion);
}

int bubbleSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Bubble);
}

int selectionSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Selection);
}

int mergeRecursiveSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Merge_recursive);
}

int mergeSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Merge);
}

int heapSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Heap);
}

int heapStdSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Heap_std);
}

int quickRecursiveSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Quick_recursive);
}

int quickRecursiveRandomSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Quick_recursive_random);
}

int quickSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Quick);
}

int quickRandomSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Quick_random);
}

int stdSortBasic(int argc, char** argv)
{
    return sortBasic(argc, argv, SortType::Std_sort);
}

int countingSortLimited(int argc, char** argv)
{
    return sortMaxLimited(argc, argv, SortType::Counting);
}

int bucketSortLimited(int argc, char** argv)
{
    return sortFloatLimited(argc, argv, SortType::Bucket);
}

void registerSortModule()
{
    registerAction("insertion_sort", insertionSortBasic, "Insertion Sort. Params: [rnd] [size] [--silent]");
    registerAction("bubble_sort", bubbleSortBasic, "Bubble Sort. Params: [rnd] [size] [--silent]");
    registerAction("selection_sort", selectionSortBasic, "Selection Sort. Params: [rnd] [size] [--silent]");
    registerAction("merge_rec_sort", mergeRecursiveSortBasic, "Merge Sort (recursive). Params: [rnd] [size] [--silent]");
    registerAction("merge_sort", mergeSortBasic, "Merge Sort. Params: [rnd] [size] [--silent]");
    registerAction("heap_sort", heapSortBasic, "Heap Sort. Params: [rnd] [size] [--silent]");
    registerAction("heap_std_sort", heapStdSortBasic, "Heap (std) Sort. Params: [rnd] [size] [--silent]");
    registerAction("quick_rec_sort", quickRecursiveSortBasic, "Quick Sort (recursive). Params: [rnd] [size] [--silent]");
    registerAction("quick_rec_rnd_sort", quickRecursiveRandomSortBasic, "Quick Sort (recursive and random). Params: [rnd] [size] [--silent]");
    registerAction("quick_sort", quickSortBasic, "Quick Sort. Params: [rnd] [size] [--silent]");
    registerAction("quick_rnd_sort", quickRandomSortBasic, "Quick Sort (random). Params: [rnd] [size] [--silent]");
    registerAction("std_sort", stdSortBasic, "Std (quick) Sort. Params: [rnd] [size] [--silent]");

    registerAction("counting_sort", countingSortLimited, "Counting (limited) sort. Params: [rnd] [size] [--silent]");
    registerAction("bucket_sort", bucketSortLimited, "Bucket (float limited) sort. Params: [rnd] [size] [--silent]");
}
