GLOBAL_CFLAGS := -Wall -std=c++11
GLOBAL_LFLAGS := -g -rdynamic

include $(CLEAR_VARS)
LOCAL_MODULE := algo
LOCAL_SRC := main.cpp \
		   	 data_structures_module.cpp \
			 search_module.cpp \
		   	 sort_module.cpp \
		   	 utils/log.cpp \
		   	 utils/timer.cpp \

LOCAL_CFLAGS := $(GLOBAL_CFLAGS)
LOCAL_LFLAGS := $(GLOBAL_LFLAGS)
LOCAL_CFLAGS += -I$(INCDIR) -I$(SRCDIR)

include $(BUILD_EXECUTABLE)
