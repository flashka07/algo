#ifndef __REGISTRAS_H_
#define __REGISTRAS_H_

#include <string>

typedef int (*ActionFunc)(int argc, char* argv[]);
struct ActionInfo {
    ActionFunc func;
    std::string description;
    ActionInfo(ActionFunc _func = nullptr, const std::string& _description = std::string())
        : func(_func),
          description(_description)
    {}
};

void registerAction(const std::string& action, ActionFunc func, const std::string& description = std::string());
void registerAction(const std::string& action, ActionInfo info);

#endif /* __REGISTRAS_H_ */
