#include "search_module.h"
#include "registras.h"

#include <algo/search.h>
#include <algo/generator.h>

#include <tuple>
#include <vector>

#include "utils/log.h"
#include "utils/printer.h"
#include "utils/timer.h"

enum SearchType
{
    Linear,
    Linear_std,
    Random,
    Binary,
};

template <class T, class ...Args>
struct first_type
{
    using type = T;
};

template <class Iter, class T>
Iter stdLinearSearch(Iter first, Iter last, const T& value)
{
    return std::find(first, last, value);
}

template <class Iter, class T, class Comp>
Iter stdLinearSearch(Iter first, Iter last, const T& value, Comp comp)
{
    return std::find_if(first, last, [&value, comp] (const T& other) { return comp(other, value); });
}

template <class ...Args>
auto searchValue(SearchType type, Args&&... args) -> typename first_type<Args...>::type
{
    switch (type) {
    case SearchType::Linear:
        return algo::linear_search(std::forward<Args>(args)...);
        break;
    case SearchType::Linear_std:
        return stdLinearSearch(std::forward<Args>(args)...);
        break;
    case SearchType::Random:
        return algo::random_search(std::forward<Args>(args)...);
        break;
    case SearchType::Binary:
        return algo::binary_search(std::forward<Args>(args)...);
        break;
    default:
        LOGE << "unknown sort type " << type;
        break;
    }
    auto argsTuple = std::forward_as_tuple(args...);
    return std::get<1>(argsTuple);
}

int searchBasic(int argc, char** argv, SearchType searchType)
{
    bool randomFill = false;
    if (argc > 0)
        randomFill = std::string(argv[0]) == "rnd";
    int size = 10;
    if (argc > 1)
        size = std::stoi(std::string(argv[1]), nullptr);
    int value = -1;
    if (argc > 2)
        value = std::stoi(std::string(argv[2]), nullptr);
    bool silent = false;
    if (argc > 3)
        silent = std::string(argv[3]) == "--silent";

    std::vector<int> data(size > 0 ? size : 10);
    algo::generator gen;
    gen.data(data.begin(), data.end(), randomFill);
    if (value == -1)
        value = gen.random_value(static_cast<size_t>(1), data.size());

    Timer timer;

    LOGI << "input - search for " << value << " in " << printSeq(data.cbegin(), data.cend(), silent);
    timer.start();
    auto iter = searchValue(searchType, data.cbegin(), data.cend(), value);
    auto elapsed = timer.elapsed();
    LOGI << "search <equal_to> finished: " << (iter == data.cend() ? "not found" : "found")
         << ". Done in " << Timer::elapsedToString(elapsed);

    timer.restart();
    iter = searchValue(searchType, data.cbegin(), data.cend(), value, std::greater<int>());
    elapsed = timer.elapsed();
    LOGI << "search <greater> finished: " << (iter == data.cend() ? "not found" : "found")
         << ". Done in " << Timer::elapsedToString(elapsed);

    return 0;
}

int searchSorted(int argc, char** argv, SearchType searchType)
{
    bool randomFill = false;
    if (argc > 0)
        randomFill = std::string(argv[0]) == "rnd";
    int size = 10;
    if (argc > 1)
        size = std::stoi(std::string(argv[1]), nullptr);
    int value = -1;
    if (argc > 2)
        value = std::stoi(std::string(argv[2]), nullptr);
    bool silent = false;
    if (argc > 3)
        silent = std::string(argv[3]) == "--silent";

    std::vector<int> data(size > 0 ? size : 10);
    algo::generator gen;
    gen.data(data.begin(), data.end(), randomFill);
    if (value == -1)
        value = gen.random_value(static_cast<size_t>(1), data.size());

    Timer timer;

    std::sort(data.begin(), data.end());
    LOGI << "input - search for " << value << " in sorted " << printSeq(data.cbegin(), data.cend(), silent);
    timer.start();
    auto iter = searchValue(searchType, data.cbegin(), data.cend(), value);
    auto elapsed = timer.elapsed();
    LOGI << "search finished: " << (iter == data.cend() ? "not found" : "found")
         << ". Done in " << Timer::elapsedToString(elapsed);

    return 0;
}

int linearSearchBasic(int argc, char** argv)
{
    return searchBasic(argc, argv, SearchType::Linear);
}

int linearStdSearchBasic(int argc, char** argv)
{
    return searchBasic(argc, argv, SearchType::Linear_std);
}

int randomSearchBasic(int argc, char** argv)
{
    return searchBasic(argc, argv, SearchType::Random);
}

int linearSearchSorted(int argc, char** argv)
{
    return searchSorted(argc, argv, SearchType::Linear);
}

int randomSearchSorted(int argc, char** argv)
{
    return searchSorted(argc, argv, SearchType::Random);
}

int binarySearchSorted(int argc, char** argv)
{
    return searchSorted(argc, argv, SearchType::Binary);
}

void registerSearchModule()
{
    registerAction("linear_search", linearSearchBasic, "Linear search. Params: [rnd] [size] [value|-1] [--silent]");
    registerAction("linear_std_search", linearStdSearchBasic, "Linear (std) search. Params: [rnd] [size] [value|-1] [--silent]");
    registerAction("random_search", randomSearchBasic, "Random search. Params: [rnd] [size] [value|-1] [--silent]");
    registerAction("linear_search_sorted", linearSearchSorted, "Linear search (sorted input). Params: [rnd] [size] [value|-1] [--silent]");
    registerAction("random_search_sorted", randomSearchSorted, "Random search (sorted input). Params: [rnd] [size] [value|-1] [--silent]");
    registerAction("binary_search_sorted", binarySearchSorted, "Binary search (sorted input). Params: [rnd] [size] [value|-1] [--silent]");
}
