#include <gtest/gtest.h>
#include <algo/sort.h>
#include <algo/generator.h>

#include <array>

TEST(sort,insertion)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::insertion_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::insertion_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::insertion_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::insertion_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::insertion_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::insertion_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::insertion_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::insertion_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::insertion_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,bubble)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::bubble_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::bubble_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::bubble_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::bubble_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::bubble_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::bubble_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::bubble_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::bubble_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::bubble_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,selection)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::selection_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::selection_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::selection_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::selection_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::selection_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::selection_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::selection_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::selection_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::selection_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,merge_recursive)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::merge_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::merge_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::merge_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::merge_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::merge_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::merge_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::merge_sort_recursive(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::merge_sort_recursive(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::merge_sort_recursive(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,merge)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::merge_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::merge_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::merge_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::merge_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::merge_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::merge_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::merge_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::merge_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::merge_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,heap)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::heap_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::heap_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::heap_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::heap_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::heap_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::heap_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::heap_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::heap_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::heap_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,quick_recursive)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::quick_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::quick_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort_recursive(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::quick_sort_recursive(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_recursive(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::quick_sort_recursive(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,quick_recursive_random)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::quick_sort_recursive_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_recursive_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort_recursive_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::quick_sort_recursive_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_recursive_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort_recursive_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::quick_sort_recursive_random(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_recursive_random(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::quick_sort_recursive_random(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,quick)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::quick_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::quick_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::quick_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::quick_sort(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,quick_random)
{
    algo::generator gen;
    std::vector<int> buf(100);
    gen.data(buf.begin(), buf.end(), false);

    algo::quick_sort_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();

    algo::quick_sort_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::quick_sort_random(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    auto comp = std::greater<int>();
    algo::quick_sort_random(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    gen.data(buf.begin(), buf.end(), true);
    algo::quick_sort_random(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));

    algo::quick_sort_random(buf.begin(), buf.end(), comp);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend(), comp));
}

TEST(sort,counting)
{
    algo::generator gen;
    const unsigned int kmax = 100;
    std::vector<unsigned int> buf(kmax);
    gen.data(buf.begin(), buf.end(), false);

    algo::counting_sort_limited(buf.begin(), buf.end(), kmax);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);

    algo::counting_sort_limited(buf.begin(), buf.end(), kmax);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::counting_sort_limited(buf.begin(), buf.end(), kmax);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();
    gen.data(buf.begin(), buf.end(), true);

    algo::counting_sort_limited(buf.begin(), buf.end(), kmax);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), true);

    algo::counting_sort_limited(buf.begin(), buf.end(), kmax);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::counting_sort_limited(buf.begin(), buf.end(), kmax);
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));
}

TEST(sort,bucket)
{
    algo::generator gen;
    std::vector<float> buf(100);
    gen.data(buf.begin(), buf.end(), 0.0f, 1.0f, false);

    algo::bucket_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), 0.0f, 1.0f, true);

    algo::bucket_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::bucket_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    buf.pop_back();
    gen.data(buf.begin(), buf.end(), 0.0f, 1.0f, true);

    algo::bucket_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    gen.data(buf.begin(), buf.end(), 0.0f, 1.0f, true);

    algo::bucket_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    algo::bucket_sort(buf.begin(), buf.end());
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));
}
