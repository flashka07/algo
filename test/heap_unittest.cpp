#include <gtest/gtest.h>
#include <algo/heap.h>

TEST(heap,basic)
{
    using algo::heap;

    heap<int> dheap;
    EXPECT_TRUE(dheap.empty());

    dheap.push(0);
    EXPECT_EQ(1, dheap.size());

    heap<int> dheap2(dheap);
    EXPECT_FALSE(dheap2.empty());

    heap<int> dheap3(std::move(dheap2));
    EXPECT_TRUE(dheap2.empty());
    EXPECT_FALSE(dheap3.empty());

    heap<int> dheap4 {0, 5, 10};
    auto tmp = {10, 5, 0};
    EXPECT_TRUE(std::equal(dheap4.cbegin(), dheap4.cend(), tmp.begin()));

    dheap2 = dheap3;
    EXPECT_EQ(1, dheap2.size());
    EXPECT_EQ(dheap2, dheap3);

    dheap3.push(1);
    EXPECT_EQ(2, dheap3.size());
    EXPECT_NE(dheap2, dheap3);

    dheap2 = std::move(dheap3);
    EXPECT_TRUE(dheap3.empty());
    EXPECT_NE(dheap2, dheap3);

    dheap4 = {1, 6, 11};
    tmp = {11, 6, 1};
    EXPECT_TRUE(std::equal(dheap4.cbegin(), dheap4.cend(), tmp.begin()));

    EXPECT_EQ(dheap4[1], dheap4.at(1));
    EXPECT_EQ(dheap4.top(), dheap4.front());

    dheap.assign(dheap4.cbegin(), dheap4.cend());
    EXPECT_EQ(dheap, dheap4);

    dheap4.assign({2, 7, 12, 17});
    tmp = {17, 7, 12, 2};
    EXPECT_TRUE(std::equal(dheap4.cbegin(), dheap4.cend(), tmp.begin()));

    dheap4.pop();
    EXPECT_EQ(3, dheap4.size());

    dheap4.set(dheap4.begin() + 1, 22);
    EXPECT_EQ(22, dheap4.top());

    dheap4.erase(dheap4.begin() + 1);
    EXPECT_EQ(2, dheap4.size());

    dheap.swap(dheap4);
    EXPECT_EQ(2, dheap.size());
    EXPECT_EQ(3, dheap4.size());

    dheap2.clear();
    EXPECT_TRUE(dheap2.empty());

    auto buf = dheap.sorted();
    EXPECT_TRUE(std::is_sorted(buf.cbegin(), buf.cend()));

    EXPECT_FALSE(dheap == dheap4);
    EXPECT_TRUE(dheap != dheap4);
    EXPECT_FALSE(dheap < dheap4);
    EXPECT_FALSE(dheap <= dheap4);
    EXPECT_TRUE(dheap > dheap4);
    EXPECT_TRUE(dheap >= dheap4);
}
