#include <gtest/gtest.h>
#include <algo/search.h>
#include <algo/generator.h>

#include <array>

TEST(search,linear)
{
    std::array<int, 100> buf;
    algo::generator::generate_sequential_data(buf.begin(), buf.end(), false);

    EXPECT_EQ(buf.cend(), algo::linear_search(buf.cbegin(), buf.cend(), -1));
    EXPECT_EQ(buf.cend() - 1, algo::linear_search(buf.cbegin(), buf.cend(), 100));
}

TEST(search,random)
{
    std::array<int, 100> buf;
    algo::generator::generate_sequential_data(buf.begin(), buf.end(), false);

    EXPECT_EQ(buf.cend(), algo::linear_search(buf.cbegin(), buf.cend(), -1));
    EXPECT_EQ(buf.cend() - 1, algo::linear_search(buf.cbegin(), buf.cend(), 100));
}

TEST(search,binary)
{
    std::array<int, 100> buf;
    algo::generator::generate_sequential_data(buf.begin(), buf.end(), false);
    // data is already sorted
    EXPECT_EQ(buf.cend(), algo::binary_search(buf.cbegin(), buf.cend(), -1));
    EXPECT_EQ(buf.cend() - 1, algo::binary_search(buf.cbegin(), buf.cend(), 100));
}
