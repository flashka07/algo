#include <gtest/gtest.h>
#include <algo/generator.h>

#include <array>

TEST(generator,basic)
{
    std::default_random_engine engine;
    engine.seed(0);

    algo::generator gen(0);
    EXPECT_EQ(engine, gen.engine());

    std::uniform_int_distribution<int> distr(1, 5);
    std::array<int, 5> buf1;
    std::generate(buf1.begin(), buf1.end(), [&engine, &distr] () { return distr(engine); });

    std::array<int, 5> buf2;
    gen.data(buf2.begin(), buf2.end(), true);
    EXPECT_EQ(buf1, buf2);

    gen.data(buf1.begin(), buf1.end(), false);
    EXPECT_NE(buf1, buf2);
    auto tmp = {5, 4, 3, 2, 1};
    EXPECT_TRUE(std::equal(buf1.cbegin(), buf1.cend(), tmp.begin()));

    gen.reset(0);
    for (auto& i : buf1)
        i = gen.random_value(1, 5);
    EXPECT_EQ(buf1, buf2);

    algo::generator::generate_sequential_data(buf1.begin(), buf1.end(), false);
    tmp = {1, 2, 3, 4, 5};
    EXPECT_TRUE(std::equal(buf1.cbegin(), buf1.cend(), tmp.begin()));

    std::uniform_real_distribution<float> distrf(1.0f, 5.0f);
    std::array<float, 5> buf3;
    std::generate(buf3.begin(), buf3.end(), [&engine, &distrf] () { return distrf(engine); });

    std::array<float, 5> buf4;
    gen.data(buf4.begin(), buf4.end(), true);
    EXPECT_EQ(buf3, buf4);

    gen.data(buf3.begin(), buf3.end(), false);
    EXPECT_NE(buf3, buf4);
    auto tmpf = {4.2f, 3.4f, 2.6f, 1.8f, 4.2f};
    for (int i=0; i<5; ++i) {
        EXPECT_FLOAT_EQ(*(tmpf.begin() + i), buf3[i]);
    }
}
