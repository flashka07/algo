LOCAL_PATH := $(call my-dir)

GTEST_DIR := googletest/googletest
GTEST_INCDIR := $(GTEST_DIR)/include

TESTS := heap_unittest utils_unittest generator_unittest \
         sort_unittest search_unittest

TEST_CFLAGS := -g -O2 -DNDEBUG -pthread
TEST_CFLAGS += -I$(INCDIR) -I$(SRCDIR) -I$(LOCAL_PATH)/$(GTEST_INCDIR)

TEST_LFLAGS := -pthread -lpthread

include $(CLEAR_VARS)
LOCAL_MODULE := heap_unittest
LOCAL_SRC := heap_unittest.cpp
LOCAL_CFLAGS := $(TEST_CFLAGS)
LOCAL_LFLAGS := $(TEST_LFLAGS)
LOCAL_STATIC_LIBS := gtest_static
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := generator_unittest
LOCAL_SRC := generator_unittest.cpp
LOCAL_CFLAGS := $(TEST_CFLAGS)
LOCAL_LFLAGS := $(TEST_LFLAGS)
LOCAL_STATIC_LIBS := gtest_static
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := sort_unittest
LOCAL_SRC := sort_unittest.cpp
LOCAL_CFLAGS := $(TEST_CFLAGS)
LOCAL_LFLAGS := $(TEST_LFLAGS)
LOCAL_STATIC_LIBS := gtest_static
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := search_unittest
LOCAL_SRC := search_unittest.cpp
LOCAL_CFLAGS := $(TEST_CFLAGS)
LOCAL_LFLAGS := $(TEST_LFLAGS)
LOCAL_STATIC_LIBS := gtest_static
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := utils_unittest
LOCAL_SRC := utils_unittest.cpp ../src/utils/timer.cpp
LOCAL_CFLAGS := $(TEST_CFLAGS)
LOCAL_LFLAGS := $(TEST_LFLAGS)
LOCAL_STATIC_LIBS := gtest_static
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := gtest_static
LOCAL_SRC := $(GTEST_DIR)/src/gtest-all.cc $(GTEST_DIR)/src/gtest_main.cc
LOCAL_CFLAGS := $(TEST_CFLAGS) -I$(LOCAL_PATH)/$(GTEST_DIR)
include $(BUILD_STATIC_LIB)

tests : $(TESTS)
clean_tests : $(TESTS:%=clean_%)
	@echo "Clean test results"
	@rm -f $(TESTS:%=$(OBJDIR)/%.result)

list_failed_tests :
	@echo "Failed tests:"
	@-grep "FAILED" $(OBJDIR)/*.result | sed 's/^.\+\/\(\w\+\)\.result:.\+/* \1/g' | uniq

run_tests : $(TESTS:%=run_%)
	@$(MAKE) list_failed_tests

define add-run-target
$(OBJDIR)/$(1).result : $(BINDIR)/$(1)
	@echo "Run test $(1)"
	@$$< | tee $$@; echo "Test $(1) finished"
run_$(1) : $(OBJDIR)/$(1).result
endef
$(foreach test,$(TESTS),$(eval $(call add-run-target,$(test))))
