#include <gtest/gtest.h>
#include <utils/timer.h>
#include <utils/printer.h>

#include <thread>
#include <sstream>
#include <array>
#include <algorithm>

TEST(utils,timer)
{
    using namespace std::chrono;

    Timer timer;
    EXPECT_FALSE(timer.isValid());

    timer.start();
    EXPECT_TRUE(timer.isValid());

    timer.invalidate();
    EXPECT_FALSE(timer.isValid());

    timer.start();
    std::this_thread::sleep_for(milliseconds(200));

    EXPECT_LT(milliseconds(190), timer.elapsedMs());
    EXPECT_LT(microseconds(190 * 1000), timer.elapsedUs());
    EXPECT_LT(nanoseconds(190 * 1000 * 1000), timer.elapsedNs());
    EXPECT_GE(seconds(1), timer.elapsedS());

    EXPECT_EQ(std::string("1000 ms"), Timer::elapsedToString(seconds(1)));
    EXPECT_EQ(std::string("5000 ms"), Timer::elapsedToString(seconds(5)));
    EXPECT_EQ(std::string("10 s"), Timer::elapsedToString(seconds(10)));

    EXPECT_EQ(std::string("1000 us"), Timer::elapsedToString(milliseconds(1)));
    EXPECT_EQ(std::string("5000 us"), Timer::elapsedToString(milliseconds(5)));
    EXPECT_EQ(std::string("10 ms"), Timer::elapsedToString(milliseconds(10)));

    EXPECT_EQ(std::string("1000 ns"), Timer::elapsedToString(microseconds(1)));
    EXPECT_EQ(std::string("5000 ns"), Timer::elapsedToString(microseconds(5)));
    EXPECT_EQ(std::string("10 us"), Timer::elapsedToString(microseconds(10)));
}

TEST(utils,printer)
{
    std::array<int, 5> buf;
    int counter = 0;
    std::generate(buf.begin(), buf.end(), [&counter] () { return ++counter; });

    std::stringstream stream;
    stream << printSeq(buf.cbegin(), buf.cend());
    EXPECT_EQ(std::string("(5): [1 2 3 4 5]"), stream.str());

    stream.str(std::string());
    stream.clear();
    stream << printSeq(buf.cbegin(), buf.cend(), true);
    EXPECT_EQ(std::string("(5): [1 .. 5]"), stream.str());
}
