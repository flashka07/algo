#ifndef __HEAP_H_
#define __HEAP_H_

#include <iterator>
#include <stdexcept>
#include <vector>

namespace algo
{

namespace __heap
{

template <class Iter>
Iter parent(const Iter& first, const Iter& item);
template <class Iter>
Iter left(const Iter& first, const Iter& item);
template <class Iter>
Iter right(const Iter& first, const Iter& item);
template <class Iter, class Comp>
void heapify(const Iter& first, const Iter& last, const Iter& item, Comp comp);
template <class Iter, class Comp>
void build(const Iter& first, const Iter& last, Comp comp);
template <class Iter, class Comp>
void set(const Iter& first, const Iter& last, Iter item, Comp comp);
template <class Iter, class Comp>
void sort(const Iter& first, Iter last, Comp comp);

}  // end of __heap namespace

template <class Iter, class Comp = std::less<decltype(*(Iter()))>>
class heap_adapter
{
public:
    using value_type = typename std::remove_reference<decltype(*(Iter()))>::type;
    using reference = typename std::add_lvalue_reference<value_type>::type;
    using const_reference = typename std::add_const<reference>::type;

    heap_adapter()
        : m_size(0)
    {
        m_memend = m_last = m_first;
    }

    heap_adapter(const Iter& memstart, const Iter& memend)
        : m_first(memstart), m_last(m_first), m_memend(memend), m_size(0)
    {}

    bool empty() const { return m_size == 0; }

    size_t size() const { return m_size; }
    size_t max_size() const { return std::distance(m_first, m_memend); }

    void attach(const Iter& memstart, const Iter& memend)
    {
        m_last = m_first = memstart;
        m_memend = memend;
        m_size = 0;
    }

    void build(const Iter& last, const Comp& comp = Comp())
    {
        if (m_first == m_memend)
            throw std::logic_error("heap_adapter::build: there is no attached buffer");

        m_last = last;
        m_comp = comp;
        m_size = std::distance(m_first, m_last);
        __heap::build(m_first, m_last, m_comp);
    }

    void build(const Iter& first, const Iter& last, const Iter& memend, const Comp& comp = Comp())
    {
        attach(first, memend);

        m_last = last;
        m_comp = comp;
        m_size = std::distance(m_first, m_last);
        __heap::build(m_first, m_last, m_comp);
    }

    void sort()
    {
        if (empty())
            return;

        m_size = 0;
        __heap::sort(m_first, m_last, m_comp);
        m_last = m_first;
    }

    const_reference top() const { return *m_first; }

    void pop()
    {
        if (empty())
            throw std::out_of_range("heap_adapter::pop: heap is empty");

        --m_size;
        m_last = std::prev(m_last);
        std::swap(*m_first, *m_last);
        __heap::heapify(m_first, m_last, m_first, m_comp);
    }

    void push(const value_type& value)
    {
        if (m_last == m_memend)
            throw std::out_of_range("heap_adapter::set: heap is full");

        ++m_size;
        auto pos = m_last;
        *pos = value;
        m_last = std::next(m_last);
        __heap::set(m_first, m_last, pos, m_comp);
    }

    void push(value_type&& value)
    {
        if (m_last == m_memend)
            throw std::out_of_range("heap_adapter::set: heap is full");

        ++m_size;
        auto pos = m_last;
        *pos = std::move(value);
        m_last = std::next(m_last);
        __heap::set(m_first, m_last, pos, m_comp);
    }

    void set(const Iter& pos, const value_type& value)
    {
        if (m_comp(value, *pos))
            throw std::invalid_argument("heap_adapter::set: invalid argument");

        *pos = value;
        __heap::set(m_first, m_last, pos, m_comp);
    }

    void set(const Iter& pos, value_type&& value)
    {
        if (m_comp(value, *pos))
            throw std::invalid_argument("heap_adapter::set: invalid argument");

        *pos = std::move(value);
        __heap::set(m_first, m_last, pos, m_comp);
    }

    void erase(const Iter& pos)
    {
        --m_size;
        m_last = std::prev(m_last);
        std::swap(*pos, *m_last);
        __heap::heapify(m_first, m_last, pos, m_comp);
    }

    Iter begin() const { return m_first; }
    Iter end() const { return m_last; }
    Iter cbegin() const { return m_first; }
    Iter cend() const { return m_last; }

private:
    Iter m_first;
    Iter m_last;
    Iter m_memend;
    Comp m_comp;
    size_t m_size;
};

template <class T, class Comp = std::less<T>, class Allocator = std::allocator<T>>
class heap
{
public:
    using value_type = T;
    using allocator_type = Allocator;
    using container_type = std::vector<T, Allocator>;
    using compare_type = Comp;

    using reference = typename container_type::reference;
    using const_reference = typename container_type::const_reference;
    using pointer = typename container_type::pointer;
    using const_pointer = typename container_type::const_pointer;

    using iterator = typename container_type::iterator;
    using const_iterator = typename container_type::const_iterator;
    using difference_type = typename container_type::difference_type;
    using size_type = typename container_type::size_type;

    explicit heap(const allocator_type& alloc = allocator_type())
        : m_cont(alloc)
    {}

    template <class Iter>
    heap(const Iter& first, const Iter& last, const allocator_type& alloc = allocator_type())
        : m_cont(first, last, alloc)
    {
        build();
    }

    heap(std::initializer_list<value_type> il, const allocator_type& alloc = allocator_type())
        : m_cont(il, alloc)
    {
        build();
    }

    heap& operator=(std::initializer_list<value_type> il)
    {
        m_cont = il;
        build();
        return *this;
    }

    const_iterator begin() const
    {
        return m_cont.begin();
    }

    const_iterator cbegin() const
    {
        return m_cont.cbegin();
    }

    const_iterator end() const
    {
        return m_cont.end();
    }

    const_iterator cend() const
    {
        return m_cont.cend();
    }

    size_type size() const
    {
        return m_cont.size();
    }

    size_type max_size() const
    {
        return m_cont.max_size();
    }

    size_type capacity() const
    {
        return m_cont.capacity();
    }

    bool empty() const
    {
        return m_cont.empty();
    }

    void reserve(size_type n)
    {
        return m_cont.reserve(n);
    }

    void shrink_to_fit()
    {
        m_cont.shrink_to_fit();
    }

    const_reference operator[](size_type n) const
    {
        return m_cont[n];
    }

    const_reference at(size_type n) const
    {
        return m_cont.at(n);
    }

    const_reference front() const
    {
        return m_cont.front();
    }

    const_reference top()
    {
        return front();
    }

    const_reference back() const
    {
        return m_cont.back();
    }

    const_pointer data() const
    {
        return m_cont.data();
    }

    template <class Iter>
    void assign(const Iter& first, const Iter& last)
    {
        m_cont.assign(first, last);
        build();
    }

    void assign(std::initializer_list<value_type> il)
    {
        m_cont.assign(il);
        build();
    }

    void push(const value_type& value)
    {
        m_cont.push_back(value);
        set_from_last();
    }

    void push(value_type&& value)
    {
        m_cont.push_back(std::move(value));
        set_from_last();
    }

    void pop()
    {
        pop_first();
        m_cont.pop_back();
    }

    template <class... Args>
    void emplace(Args&&... args)
    {
        m_cont.emplace_back(std::forward<Args>(args)...);
        set_from_last();
    }

    void set(const const_iterator& iter, const value_type& value)
    {
        auto pos = to_iter(iter);
        *pos = value;
        set_pos(pos);
    }

    void set(const const_iterator& iter, value_type&& value)
    {
        auto pos = to_iter(iter);
        *pos = std::move(value);
        set_pos(pos);
    }

    void erase(const const_iterator& iter)
    {
        auto pos = to_iter(iter);
        erase_pos(pos);
        m_cont.pop_back();
    }

    void swap(heap& other)
    {
        m_cont.swap(other.m_cont);
    }

    void clear()
    {
        m_cont.clear();
    }

    container_type sorted() const &
    {
        return heap(*this).sorted();
    }

    container_type sorted() &&
    {
        __heap::sort(m_cont.begin(), m_cont.end(), compare_type());
        return std::move(m_cont);
    }

    template <class Iter>
    void sortTo(const Iter& first, const Iter& last) const
    {
        std::copy(m_cont.cbegin(), m_cont.cend(), first);
        __heap::sort(first, last, compare_type());
    }

    allocator_type get_allocator() const
    {
        return m_cont.get_allocator();
    }

    template <class U, class UComp, class UAlloc>
    friend bool operator==(const heap<U, UComp, UAlloc>& lhs, const heap<U, UComp, UAlloc>& rhs);
    template <class U, class UComp, class UAlloc>
    friend bool operator!=(const heap<U, UComp, UAlloc>& lhs, const heap<U, UComp, UAlloc>& rhs);
    template <class U, class UComp, class UAlloc>
    friend bool operator<(const heap<U, UComp, UAlloc>& lhs, const heap<U, UComp, UAlloc>& rhs);
    template <class U, class UComp, class UAlloc>
    friend bool operator<=(const heap<U, UComp, UAlloc>& lhs, const heap<U, UComp, UAlloc>& rhs);
    template <class U, class UComp, class UAlloc>
    friend bool operator>(const heap<U, UComp, UAlloc>& lhs, const heap<U, UComp, UAlloc>& rhs);
    template <class U, class UComp, class UAlloc>
    friend bool operator>=(const heap<U, UComp, UAlloc>& lhs, const heap<U, UComp, UAlloc>& rhs);

private:
    void build()
    {
        __heap::build(m_cont.begin(), m_cont.end(), compare_type());
    }

    void set_from_last()
    {
        auto last = std::prev(m_cont.end());
        __heap::set(m_cont.begin(), m_cont.end(), last, compare_type());
    }

    void set_pos(const iterator& pos)
    {
        __heap::set(m_cont.begin(), m_cont.end(), pos, compare_type());
    }

    void pop_first()
    {
        auto last = std::prev(m_cont.end());
        std::swap(*m_cont.begin(), *last);
        __heap::heapify(m_cont.begin(), last, m_cont.begin(), compare_type());
    }

    void erase_pos(const iterator& pos)
    {
        auto last = std::prev(m_cont.end());
        std::swap(*pos, *last);
        __heap::heapify(m_cont.begin(), m_cont.end(), pos, compare_type());
    }

    iterator to_iter(const const_iterator& iter)
    {
        auto pos = m_cont.begin();
        std::advance(pos, std::distance(cbegin(), iter));
        return pos;
    }

    container_type m_cont;
};

template <class T, class Comp, class Alloc>
void swap(heap<T, Comp, Alloc>& lhs, heap<T, Comp, Alloc>& rhs)
{
    lhs.swap(rhs);
}

template <class T, class Comp, class Alloc>
bool operator==(const heap<T, Comp, Alloc>& lhs, const heap<T, Comp, Alloc>& rhs)
{
    return lhs.m_cont == rhs.m_cont;
}

template <class T, class Comp, class Alloc>
bool operator!=(const heap<T, Comp, Alloc>& lhs, const heap<T, Comp, Alloc>& rhs)
{
    return lhs.m_cont != rhs.m_cont;
}

template <class T, class Comp, class Alloc>
bool operator<(const heap<T, Comp, Alloc>& lhs, const heap<T, Comp, Alloc>& rhs)
{
    return lhs.m_cont < rhs.m_cont;
}

template <class T, class Comp, class Alloc>
bool operator<=(const heap<T, Comp, Alloc>& lhs, const heap<T, Comp, Alloc>& rhs)
{
    return lhs.m_cont <= rhs.m_cont;
}

template <class T, class Comp, class Alloc>
bool operator>(const heap<T, Comp, Alloc>& lhs, const heap<T, Comp, Alloc>& rhs)
{
    return lhs.m_cont > rhs.m_cont;
}

template <class T, class Comp, class Alloc>
bool operator>=(const heap<T, Comp, Alloc>& lhs, const heap<T, Comp, Alloc>& rhs)
{
    return lhs.m_cont >= rhs.m_cont;
}

namespace __heap
{

inline size_t parent(size_t idx)
{
    return (idx - 1) / 2;
}

inline size_t left(size_t idx)
{
    return 2 * idx + 1;
}

inline size_t right(size_t idx)
{
    return 2 * idx + 2;
}

template <class Iter>
Iter parent(const Iter& first, size_t idx)
{
    Iter result = first;
    std::advance(result, parent(idx));
    return result;
}

template <class Iter>
Iter parent(const Iter& first, const Iter& item)
{
    return parent(first, std::distance(first, item));
}

template <class Iter>
Iter left(const Iter& first, size_t idx)
{
    Iter result = first;
    std::advance(result, left(idx));
    return result;
}

template <class Iter>
Iter left(const Iter& first, const Iter& item)
{
    return left(first, std::distance(first, item));
}

template <class Iter>
Iter right(const Iter& first, size_t idx)
{
    Iter result = first;
    std::advance(result, right(idx));
    return result;
}

template <class Iter>
Iter right(const Iter& first, const Iter& item)
{
    return right(first, std::distance(first, item));
}

template <class Iter, class Comp>
void heapify(const Iter& first, const Iter& last, const Iter& item, Comp comp)
{
    auto leftItem = left(first, item);
    auto rightItem = right(first, item);
    Iter largest = item;
    if (leftItem < last && comp(*largest, *leftItem))
        largest = leftItem;
    if (rightItem < last && comp(*largest, *rightItem))
        largest = rightItem;
    if (largest != item) {
        std::swap(*item, *largest);
        heapify(first, last, largest, comp);
    }
}

template <class Iter, class Comp>
void build(const Iter& first, const Iter& last, Comp comp)
{
    auto size = std::distance(first, last);
    if (!size)
        return;

    size_t middlePos = (size == 1) ? 0 : size / 2 - 1;
    Iter middle = first;
    std::advance(middle, middlePos);
    while (true) {
        heapify(first, last, middle, comp);
        if (middle == first)
            break;
        else
            middle = std::prev(middle);
    }
}

template <class Iter, class Comp>
void set(const Iter& first, const Iter& last, Iter item, Comp comp)
{
    while (item != first) {
        Iter parentItem = parent(first, item);
        if (!comp(*parentItem, *item))
            break;
        std::swap(*item, *parentItem);
        item = parentItem;
    }
}

template <class Iter, class Comp>
void sort(const Iter& first, Iter last, Comp comp)
{
    for (Iter item = std::prev(last); item != first; item = std::prev(item)) {
        std::swap(*first, *item);
        last = std::prev(last);
        __heap::heapify(first, last, first, comp);
    }
}

}  // end of __heap namespace

}  // end of algo namespace

#endif /* __HEAP_H_ */
