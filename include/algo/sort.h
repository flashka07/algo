#ifndef __SORT_H_
#define __SORT_H_

#include <cmath>
#include <iterator>
#include <forward_list>
#include <functional>
#include <limits>
#include <stdexcept>
#include <vector>

#include <algo/heap.h>
#include <algo/generator.h>

namespace algo
{

// Usable sort algorithms:
// template <class Iter, class Comp>
// void insertion_sort(Iter first, Iter last, Comp comp);
// template <class Iter>
// void insertion_sort(Iter first, Iter last);

// template <class Iter, class Comp>
// void bubble_sort(Iter first, Iter last, Comp comp);
// template <class Iter>
// void bubble_sort(Iter first, Iter last);

// template <class Iter, class Comp>
// void selection_sort(Iter first, Iter last, Comp comp);
// template <class Iter>
// void selection_sort(Iter first, Iter last);

// template <class Iter, class Comp>
// void merge_sort_recursive(Iter first, Iter last, Comp comp);
// template <class Iter>
// void merge_sort_recursive(Iter first, Iter last);

// template <class Iter, class Comp>
// void merge_sort(Iter first, Iter last, Comp comp);
// template <class Iter>
// void merge_sort(Iter first, Iter last);

// template <class Iter, class Comp>
// void heap_sort(Iter first, Iter last, Comp comp);
// template <class Iter>
// void heap_sort(Iter first, Iter last);

// template <class Iter, class Comp>
// void quick_sort_recursive(Iter first, Iter last, Comp comp);
// template <class Iter>
// void quick_sort_recursive(Iter first, Iter last);

// template <class Iter, class Comp>
// void quick_sort_recursive_random(Iter first, Iter last, Comp comp);
// template <class Iter>
// void quick_sort_recursive_random(Iter first, Iter last);

// template <class Iter, class Comp>
// void quick_sort(Iter first, Iter last, Comp comp);
// template <class Iter>
// void quick_sort(Iter first, Iter last);

// template <class Iter, class Comp>
// void quick_sort_random(Iter first, Iter last, Comp comp);
// template <class Iter>
// void quick_sort_random(Iter first, Iter last);


// template <class Iter, class OutIter, class Key>
// void counting_sort_limited_to(Iter first, Iter last, OutIter result, size_t max, Key key);
// template <class Iter, class OutIter>
// void counting_sort_limited_to(Iter first, Iter last, OutIter result, size_t max);
// template <class Iter, class OutIter, class Key>
// void counting_sort_to(Iter first, Iter last, OutIter result, Key key);
// template <class Iter, class OutIter>
// void counting_sort_to(Iter first, Iter last, OutIter result);
// template <class Iter, class Key>
// void counting_sort_limited(Iter first, Iter last, size_t max, Key key);
// template <class Iter, class Key>
// void counting_sort(Iter first, Iter last, Key key);
// template <class Iter>
// void counting_sort_limited(Iter first, Iter last, size_t max);
// template <class Iter>
// void counting_sort(Iter first, Iter last);

// template <class Iter, class Key>
// void bucket_sort(Iter first, Iter last, Key key);
// template <class Iter>
// void bucket_sort(Iter first, Iter last);


template <class Iter, class Comp>
void insertion_sort(const Iter& first, const Iter& last, Comp comp)
{
    for (Iter key = std::next(first); key != last; key = std::next(key)) {
        auto value = *key;
        Iter i = std::prev(key);
        bool stop = false;
        do {
            if (!comp(value, *i))
                break;
            *std::next(i) = *i;
            stop = i == first;
            i = std::prev(i);
        } while (!stop);
        *std::next(i) = value;
    }
}

template <class Iter>
void insertion_sort(const Iter& first, const Iter& last)
{
    insertion_sort(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp>
void bubble_sort(const Iter& first, const Iter& last, Comp comp)
{
    auto size = std::distance(first, last) - 1;
    for (decltype(size) i = 0; i < size; ++i) {
        Iter start = first;
        Iter stop = start;
        std::advance(stop, size - i);
        while (start != stop) {
            Iter check = std::next(start);
            if (comp(*check, *start))
                std::swap(*start, *check);
            start = std::next(start);
        }
    }
}

template <class Iter>
void bubble_sort(const Iter& first, const Iter& last)
{
    bubble_sort(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp>
void selection_sort(Iter first, const Iter& last, Comp comp)
{
    auto size = std::distance(first, last) - 1;
    for (decltype(size) i = 0; i < size; ++i, first = std::next(first)) {
        Iter best = first;
        Iter start = std::next(first);
        while (start != last) {
            if (comp(*start, *best))
                best = start;
            start = std::next(start);
        }
        if (best != first)
            std::swap(*first, *best);
    }
}

template <class Iter>
void selection_sort(const Iter& first, const Iter& last)
{
    selection_sort(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp, class T>
void __merge_sort_merge(Iter first, const Iter& middle, const Iter& last, Comp comp, std::vector<T>& buffer)
{
    Iter destination = first;
    Iter second = middle;
    while (first != middle && second != last) {
        if (comp(*first, *second)) {
            buffer.push_back(*first);
            first = std::next(first);
        } else {
            buffer.push_back(*second);
            second = std::next(second);
        }
    }
    std::copy(first, middle, std::back_inserter(buffer));
    std::copy(second, last, std::back_inserter(buffer));
    std::copy(buffer.cbegin(), buffer.cend(), destination);

    buffer.clear();
}

template <class Iter, class Comp, class T>
void __merge_sort_recursive(const Iter& first, const Iter& last, Comp comp, std::vector<T>& buffer)
{
    auto size = std::distance(first, last);
    if (size > 0) {
        if (size == 1)
            return;
        decltype(size) middlePos = size / 2;
        Iter middle = first;
        std::advance(middle, middlePos);
        __merge_sort_recursive(first, middle, comp, buffer);
        __merge_sort_recursive(middle, last, comp, buffer);
        __merge_sort_merge(first, middle, last, comp, buffer);
    }
}

template <class Iter, class Comp>
void merge_sort_recursive(const Iter& first, const Iter& last, Comp comp)
{
    auto value = *first;
    std::vector<decltype(value)> buffer;
    buffer.reserve(std::distance(first, last));

    __merge_sort_recursive(first, last, comp, buffer);
}

template <class Iter>
void merge_sort_recursive(const Iter& first, const Iter& last)
{
    merge_sort_recursive(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp, class T>
void __merge_sort_loop(const Iter& first, const Iter& last, Comp comp, std::vector<T>& buffer)
{
    auto size = std::distance(first, last);
    if (size <= 1)
        return;

    size_t levels = std::log2(size);
    size_t iters = 1u << levels;
    if (size > iters) {
        auto start = first;
        size_t reminder = size % iters;
        size_t acc = 0;
        for (size_t i = 0; i < iters; ++i, start = std::next(start)) {
            acc += reminder;
            if (acc >= iters) {
                auto middle = std::next(start);
                auto stop = std::next(middle);
                __merge_sort_merge(start, middle, stop, comp, buffer);
                start = std::next(start);
                acc -= iters;
            }
        }
    }

    size_t chunk_size = 1;
    for (size_t l = levels; l > 0; --l) {
        iters = 1u << l;
        chunk_size = size / iters;
        size_t reminder = size % iters;
        size_t acc = 0;
        auto start = first;
        auto next_pos = [chunk_size, &acc, reminder, iters] (const Iter& iter)
            {
                acc += reminder;
                auto offset = (acc >= iters) ? (acc -= iters, 1) : 0;
                return std::next(iter, chunk_size + offset);
            };
        for (size_t i = 1; i <= iters; i += 2) {
            auto middle = next_pos(start);
            auto stop = next_pos(middle);
            __merge_sort_merge(start, middle, stop, comp, buffer);
            start = stop;
        }
    }
}

template <class Iter, class Comp>
void merge_sort(const Iter& first, const Iter& last, Comp comp)
{
    auto value = *first;
    std::vector<decltype(value)> buffer;
    buffer.reserve(std::distance(first, last));

    __merge_sort_loop(first, last, comp, buffer);
}

template <class Iter>
void merge_sort(const Iter& first, const Iter& last)
{
    merge_sort(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp>
void heap_sort(const Iter& first, const Iter& last, Comp comp)
{
    heap_adapter<Iter, Comp> heap(first, last);
    heap.build(last, comp);
    heap.sort();
}

template <class Iter>
void heap_sort(const Iter& first, const Iter& last)
{
    heap_sort(first, last, std::less<decltype(*first)>());
}

struct __quick_sort_partition
{
    template <class Iter, class Comp>
    Iter operator()(Iter first, Iter last, Comp comp)
    {
        last = std::prev(last);
        auto value = *last;
        auto edge = std::prev(first);
        while (first != last) {
            if (!comp(value, *first)) {
                edge = std::next(edge);
                std::swap(*edge, *first);
            }
            first = std::next(first);
        }
        edge = std::next(edge);
        std::swap(*edge, *last);
        return edge;
    }
};

struct __quick_sort_partition_random
{
    template <class Iter, class Comp>
    Iter operator()(const Iter& first, const Iter& last, Comp comp)
    {
        size_t max = std::distance(first, last) - 1;
        size_t r = gen.random_value<size_t>(0, max);
        std::swap(*std::next(first, r), *std::prev(last));
        return __quick_sort_partition()(first, last, comp);
    }
private:
    generator gen;
};

template <class Iter, class Comp, class Partition>
void __quick_sort_recursive(Iter first, Iter last, Comp comp, Partition& partition)
{
    while (first < last) {
        auto point = partition(first, last, comp);
        if (std::distance(first, point) < std::distance(std::next(point), last)) {
            __quick_sort_recursive(first, point, comp, partition);
            first = std::next(point);
        } else {
            __quick_sort_recursive(std::next(point), last, comp, partition);
            last = point;
        }
    }
}

template <class Iter, class Comp>
void quick_sort_recursive(const Iter& first, const Iter& last, Comp comp)
{
    __quick_sort_partition partition;
    __quick_sort_recursive(first, last, comp, partition);
}

template <class Iter>
void quick_sort_recursive(const Iter& first, const Iter& last)
{
    quick_sort_recursive(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp>
void quick_sort_recursive_random(const Iter& first, const Iter& last, Comp comp)
{
    __quick_sort_partition_random partition;
    __quick_sort_recursive(first, last, comp, partition);
}

template <class Iter>
void quick_sort_recursive_random(const Iter& first, const Iter& last)
{
    quick_sort_recursive(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp, class Partition>
void __quick_sort_loop(Iter first, Iter last, Comp comp, Partition partition)
{
    std::vector<Iter> points;
    points.reserve(std::distance(first, last));
    points.push_back(first);
    points.push_back(last);
    while (!points.empty()) {
        last = points.back(); points.pop_back();
        first = points.back(); points.pop_back();
        if (first >= last)
            continue;

        auto point = partition(first, last, comp);
        // second call
        points.push_back(std::next(point));
        points.push_back(last);
        // first call
        points.push_back(first);
        points.push_back(point);
    }
}

template <class Iter, class Comp>
void quick_sort(const Iter& first, const Iter& last, Comp comp)
{
    __quick_sort_loop(first, last, comp, __quick_sort_partition());
}

template <class Iter>
void quick_sort(const Iter& first, const Iter& last)
{
    quick_sort(first, last, std::less<decltype(*first)>());
}

template <class Iter, class Comp>
void quick_sort_random(const Iter& first, const Iter& last, Comp comp)
{
    __quick_sort_loop(first, last, comp, __quick_sort_partition_random());
}

template <class Iter>
void quick_sort_random(const Iter& first, const Iter& last)
{
    quick_sort(first, last, std::less<decltype(*first)>());
}

struct __default_key
{
    template <class T>
    T operator()(T value) const
    {
        return value;
    }
};

template <class Iter, class OutIter, class Key>
void counting_sort_limited_to(const Iter& first, const Iter& last, const OutIter& result, size_t max, Key key)
{
    using value_type = typename std::remove_reference<decltype(*first)>::type;
    using key_type = typename std::remove_reference<typename std::result_of<Key(value_type)>::type>::type;
    static_assert(std::is_unsigned<key_type>::value, "Key's type must be unsigned");

    std::vector<size_t> count(max + 1, 0);
    std::for_each(
        first, last, [&count, key] (const value_type& value)
        {
            ++count[key(value)];
        });

    std::transform(
        count.cbegin(), std::prev(count.cend()), std::next(count.cbegin()),
        std::next(count.begin()),
        [] (size_t previous, size_t current)
        {
            return previous + current;
        });

    auto stop = last;
    while (stop != first) {
        stop = std::prev(stop);
        key_type kvalue = key(*stop);
        auto pos = std::next(result, --count[kvalue]);
        *pos = *stop;
    }
}

template <class Iter, class OutIter>
void counting_sort_limited_to(const Iter& first, const Iter& last, const OutIter& result, size_t max)
{
    counting_sort_limited_to(first, last, result, max, __default_key());
}

template <class Iter, class OutIter, class Key>
void counting_sort_to(const Iter& first, const Iter& last, const OutIter& result, Key key)
{
    using value_type = typename std::remove_reference<decltype(*first)>::type;
    using key_type = typename std::remove_reference<typename std::result_of<Key(value_type)>::type>::type;
    counting_sort_limited_to(first, last, result, std::numeric_limits<key_type>::max(), key);
}

template <class Iter, class OutIter>
void counting_sort_to(const Iter& first, const Iter& last, const OutIter& result)
{
    counting_sort_to(first, last, result, __default_key());
}

template <class Iter, class Key>
void counting_sort_limited(const Iter& first, const Iter& last, size_t max, Key key)
{
    using value_type = typename std::remove_reference<decltype(*first)>::type;
    using key_type = typename std::remove_reference<typename std::result_of<Key(value_type)>::type>::type;
    static_assert(std::is_unsigned<key_type>::value, "Key's type must be unsigned");

    std::vector<size_t> count(max + 1, 0);
    std::for_each(
        first, last, [&count, key] (const value_type& value)
        {
            ++count[key(value)];
        });

    std::transform(
        count.cbegin(), std::prev(count.cend()), std::next(count.cbegin()),
        std::next(count.begin()),
        [] (size_t previous, size_t current)
        {
            return previous + current;
        });

    std::vector<size_t> positions(count);
    auto start = first;
    size_t idx = 0;
    while (start != last) {
        key_type kvalue = key(*start);
        bool placed = (kvalue > 0) ? (positions[kvalue - 1] <= idx) : true
            && idx < positions[kvalue];
        if (placed) {
            start = std::next(start);
            ++idx;
        } else {
            std::swap(*start, *std::next(first, --count[kvalue]));
        }
    }
}

template <class Iter, class Key>
void counting_sort(const Iter& first, const Iter& last, Key key)
{
    using value_type = typename std::remove_reference<decltype(*first)>::type;
    using key_type = typename std::remove_reference<typename std::result_of<Key(value_type)>::type>::type;
    counting_sort_limited(first, last, std::numeric_limits<key_type>::max(), key);
}

template <class Iter>
void counting_sort_limited(const Iter& first, const Iter& last, size_t max)
{
    counting_sort_limited(first, last, max, __default_key());
}

template <class Iter>
void counting_sort(const Iter& first, const Iter& last)
{
    counting_sort(first, last, __default_key());
}

template <class Iter, class Key>
void bucket_sort(Iter first, const Iter& last, Key key)
{
    using value_type = typename std::remove_reference<decltype(*first)>::type;
    using key_type = typename std::remove_reference<typename std::result_of<Key(value_type)>::type>::type;
    static_assert(std::is_floating_point<key_type>::value, "Key's type must be unsigned");

    auto size = std::distance(first, last);
    std::vector<std::forward_list<value_type>> buckets(size);
    std::for_each(first, last, [&buckets, key, size] (const value_type& value)
                  {
                      key_type kvalue = key(value);
                      if (kvalue < 0 || kvalue >= 1)
                          throw std::out_of_range("key value must be >= 0 and < 1");
                      buckets[size * kvalue].push_front(value);
                  });
    for (auto& bucket : buckets) {
        bucket.sort([key] (const value_type& lhs, const value_type& rhs) { return key(lhs) < key(rhs); });
        first = std::move(bucket.begin(), bucket.end(), first);
    }
}

template <class Iter>
void bucket_sort(const Iter& first, const Iter& last)
{
    bucket_sort(first, last, __default_key());
}

} // end of algo namespace

#endif /* __SORT_H_ */
