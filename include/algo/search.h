#ifndef __SEARCH_H_
#define __SEARCH_H_

#include <iterator>
#include <functional>
#include <set>

#include <algo/generator.h>

namespace algo
{

// Usable search algorithms:
// template <class Iter, class T, class Comp>
// Iter linear_search(Iter first, Iter last, const T& value, Comp comp);
// template <class Iter, class T>
// Iter linear_search(Iter first, Iter last, const T& value);

// template <class Iter, class T, class Comp>
// Iter random_search(Iter first, Iter last, const T& value, Comp comp);
// template <class Iter, class T>
// Iter random_search(Iter first, Iter last, const T& value);

// template <class Iter, class T, class Comp, class CompEq>
// Iter binary_search(Iter first, Iter last, const T& value, Comp comp, CompEq eq);
// template <class Iter, class T, class Comp>
// Iter binary_search(Iter first, Iter last, const T& value, Comp comp);
// template <class Iter, class T>
// Iter binary_search(Iter first, Iter last, const T& value);


template <class Iter, class T, class Comp>
Iter linear_search(Iter first, Iter last, const T& value, Comp comp)
{
    while (first != last) {
        if (comp(*first, value))
            break;
        first = std::next(first);
    }
    return first;
}

template <class Iter, class T>
Iter linear_search(Iter first, Iter last, const T& value)
{
    return linear_search(first, last, value, std::equal_to<T>());
}

template <class Iter, class T, class Comp>
Iter random_search(Iter first, Iter last, const T& value, Comp comp)
{
    std::set<size_t> visited;
    generator gen;

    auto size = std::distance(first, last);
    if (!size)
        return last;

    auto distr = gen.random_value_gen<size_t>(0, size - 1);
    while (visited.size() != size) {
        auto i = distr.get();
        Iter check = first;
        std::advance(check, i);
        if (comp(*check, value))
            return check;
        visited.insert(i);
    }

    return last;
}

template <class Iter, class T>
Iter random_search(Iter first, Iter last, const T& value)
{
    return random_search(first, last, value, std::equal_to<T>());
}

template <class Iter, class T, class Comp, class CompEq>
Iter binary_search(Iter first, Iter last, const T& value, Comp comp, CompEq eq)
{
    Iter result = last;
    while (first != last) {
        auto size = std::distance(first, last);
        decltype(size) middlePos = size / 2;
        Iter middle = first;
        std::advance(middle, middlePos);

        if (comp(value, *middle)) {
            last = middle;
        } else if (eq(*middle, value)) {
            result = middle;
            break;
        } else {
            if (size == 1)
                break;
            first = middle;
        }
    }
    return result;
}

template <class Iter, class T, class Comp>
Iter binary_search(Iter first, Iter last, const T& value, Comp comp)
{
    return binary_search(first, last, value, comp, [comp] (const T& left, const T& right) {
            return !comp(left, right) && !comp(right, left);
        });
}

template <class Iter, class T>
Iter binary_search(Iter first, Iter last, const T& value)
{
    return binary_search(first, last, value, std::less<T>(), std::equal_to<T>());
}

} // end of algo namespace

#endif /* __SEARCH_H_ */
