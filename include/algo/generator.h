#ifndef __GENERATOR_H_
#define __GENERATOR_H_

#include <iterator>
#include <algorithm>
#include <random>
#include <type_traits>

#include <chrono>

namespace algo
{

template <class T>
class distribution;

class generator
{
public:
    generator()
    {
        auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        m_engine.seed(seed);
    }

    template <class Seed>
    generator(Seed&& seed)
    {
        m_engine.seed(seed);
    }

    std::default_random_engine& engine()
    {
        return m_engine;
    }

    template <class Iter>
    void random_data(Iter first, Iter last)
    {
        generate_random_data(m_engine, first, last);
    }

    template <class Iter, class T>
    void random_data(Iter first, Iter last, const T& vmin, const T& vmax)
    {
        generate_random_data(first, last, vmin, vmax);
    }

    template <class Iter>
    void data(Iter first, Iter last, bool randomize)
    {
        if (randomize)
            random_data(first, last);
        else
            generate_sequential_data(first, last);
    }

    template <class Iter, class T>
    void data(Iter first, Iter last, const T& vmin, const T& vmax, bool randomize)
    {
        if (randomize)
            random_data(first, last, vmin, vmax);
        else
            generate_sequential_data(first, last, vmin, vmax);
    }

    template <class T>
    T random_value(const T& vmin, const T& vmax)
    {
        return generate_random_value(m_engine, vmin, vmax);
    }

    template <class T>
    distribution<T> random_value_gen(const T& vmin, const T& vmax)
    {
        return distribution<T>(*this, vmin, vmax);
    }

    void reset()
    {
        m_engine.seed();
    }

    template <class Seed>
    void reset(Seed&& seed)
    {
        m_engine.seed(seed);
    }

    template <class Engine, class Iter, class T>
    static void generate_random_data(Engine&& engine, Iter first, Iter last, const T& vmin, const T& vmax)
    {
        using DataType = typename std::remove_reference<decltype(*first)>::type;
        using Distribution = typename std::conditional<std::is_integral<DataType>::value,
                                                       std::uniform_int_distribution<DataType>,
                                                       std::uniform_real_distribution<DataType>>::type;
        Distribution distribution(vmin, vmax);
        std::generate(first, last, [&engine, &distribution] () {
                return distribution(engine);
            });
    }

    template <class Engine, class Iter>
    static void generate_random_data(Engine&& engine, Iter first, Iter last)
    {
        auto size = std::distance(first, last);
        generate_random_data(engine, first, last, static_cast<decltype(size)>(1), size);
    }

    template <class Iter, class T>
    static void generate_random_data(Iter first, Iter last, const T& vmin, const T& vmax)
    {
        generator gen;
        generate_random_data(gen.engine(), first, last, vmin, vmax);
    }

    template <class Iter>
    static void generate_random_data(Iter first, Iter last)
    {
        generator gen;
        generate_random_data(gen.engine(), first, last);
    }

    template <class Iter, class T>
    static void generate_sequential_data(Iter first, Iter last, const T& vmin, const T& vmax, bool indirect = true)
    {
        auto value = *first;
        if (std::is_integral<decltype(value)>::value) {
            if (indirect) {
                value = vmax;
                std::generate(first, last, [&value, vmin, vmax] ()
                              {
                                  if (value < vmin)
                                      value = vmax;
                                  return value--;
                              });
            } else {
                value = vmin;
                std::generate(first, last, [&value, vmin, vmax] ()
                              {
                                  if (value > vmax)
                                      value = vmin;
                                  return value++;
                              });

            }
        } else {
            decltype(value) vfmin = vmin, vfmax = vmax;
            decltype(value) step = (vfmax - vfmin) / std::distance(first, last);
            if (indirect) {
                value = vfmax;
                std::generate(first, last, [&value, vfmin, vfmax, step] ()
                              {
                                  if (value - vfmin < step)
                                      value = vfmax;
                                  value -= step;
                                  return value;
                              });
            } else {
                value = vfmin;
                std::generate(first, last, [&value, vfmin, vfmax, step] ()
                              {
                                  if (vfmax - value < step)
                                      value = vfmin;
                                  value += step;
                                  return value;
                              });
            }
        }
    }

    template <class Iter>
    static void generate_sequential_data(Iter first, Iter last, bool indirect = true)
    {
        auto size = std::distance(first, last);
        generate_sequential_data(first, last, static_cast<decltype(size)>(1), size, indirect);
    }

    template <class Iter, class T>
    static void generate_data(Iter first, Iter last, const T& vmin, const T& vmax, bool randomize)
    {
        if (randomize)
            generate_random_data(first, last, vmin, vmax);
        else
            generate_sequential_data(first, last, vmin, vmax);
    }

    template <class Iter>
    static void generate_data(Iter first, Iter last, bool randomize)
    {
        if (randomize)
            generate_random_data(first, last);
        else
            generate_sequential_data(first, last);
    }

    template <class Engine, class T>
    static T generate_random_value(Engine&& engine, const T& vmin, const T& vmax)
    {
        std::uniform_int_distribution<T> distribution(vmin, vmax);
        return distribution(engine);
    }

    template <class T>
    static T generate_random_value(const T& vmin, const T& vmax)
    {
        generator gen;
        return generate_random_value(gen.engine(), vmin, vmax);
    }

private:
    std::default_random_engine m_engine;
};

template <class T>
class distribution
{
public:
    distribution(generator& gen, const T& min, const T& max)
        : m_gen(gen), m_distr(min, max)
    {}

    T get()
    {
        return m_distr(m_gen.engine());
    }

private:
    generator& m_gen;
    std::uniform_int_distribution<T> m_distr;
};

}  // end of algo namespace

#endif /* __GENERATOR_H_ */
