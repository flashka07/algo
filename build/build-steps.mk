$(eval all : $(LOCAL_MODULE))

define _COMPILE_ONE
$(1) : $(2) $(call my-parent-makefile)
	@echo "$(LOCAL_MODULE) <= compile $$(notdir $$<)"
	@mkdir -p $$(dir $$@)
	@$(CC) $$(call d_cflags,$$@) $(CFLAGS) $(LOCAL_CFLAGS) -c $$< -o $$@
endef

define _COMPILE
$(foreach obj,$($(LOCAL_MODULE)_OBJS), $(eval $(call _COMPILE_ONE,$(obj),$(call my-parent-abs-dir)/$(filter $(obj:$(OBJDIR)/$(LOCAL_MODULE)/%.o=%)%,$($(LOCAL_MODULE)_SRC)))))
endef

define _LINK_EXECUTABLE
$(BINDIR)/$(LOCAL_MODULE) : $$($(LOCAL_MODULE)_OBJS) $(call module-static-lib-files,$(LOCAL_STATIC_LIBS))
	@mkdir -p $(BINDIR)
	@echo "$(LOCAL_MODULE) << executable"
	@$(LINKER) $$@ $(LFLAGS) $(LOCAL_LFLAGS) $$^
endef

define _LINK_STATIC
$(OBJDIR)/$(call module-name-to-static-lib,$(LOCAL_MODULE)) : $$($(LOCAL_MODULE)_OBJS) $(call module-static-lib-files,$(LOCAL_STATIC_LIBS))
	@echo "$(LOCAL_MODULE) << static lib"
	@$(AR) $(ARFLAGS) $$@ $$^
endef

define _CLEAN
clean_$(LOCAL_MODULE) :
	@echo "$(LOCAL_MODULE) << clean"
	@rm -f $(1) $$($(LOCAL_MODULE)_OBJS) $$($(LOCAL_MODULE)_OBJS:%.o=%.d)
endef

$(eval $(LOCAL_MODULE)_SRC := $(LOCAL_SRC))
$(eval $(LOCAL_MODULE)_OBJS := $(patsubst %.cpp,%.o,$(LOCAL_SRC)))
$(eval $(LOCAL_MODULE)_OBJS := $(patsubst %.cc,%.o,$($(LOCAL_MODULE)_OBJS)))
$(eval $(LOCAL_MODULE)_OBJS := $(addprefix $(OBJDIR)/$(LOCAL_MODULE)/,$($(LOCAL_MODULE)_OBJS)))
$(eval -include $$($(LOCAL_MODULE)_OBJS:%.o=%.d))
$(eval clean: clean_$(LOCAL_MODULE))
