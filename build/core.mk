abs-dir = $(shell dirname $(realpath $(1)))
my-dir = $(call abs-dir,$(lastword $(MAKEFILE_LIST)))
my-parent-makefile-path = $(lastword $(filter %Makefile.mk,$(MAKEFILE_LIST)))
my-parent-abs-dir = $(call abs-dir, $(call my-parent-makefile-path))
my-parent-makefile = $(realpath $(call my-parent-makefile-path))

BUILDDIR := $(call my-dir)
PROJROOT := $(abspath $(BUILDDIR)/..)
my-dir-rel = $(patsubst $(shell pwd)%,.%,$(call my-dir))

OBJDIR := $(PROJROOT)/obj
BINDIR := $(PROJROOT)/bin
SRCDIR := $(PROJROOT)/src
INCDIR := $(PROJROOT)/include

CC := g++
CFLAGS := -Wall -std=c++11
LINKER := g++ -o
LFLAGS := -g -rdynamic
AR := ar
ARFLAGS := rcs

ifneq ($(DEBUG),)
OBJDIR := $(OBJDIR)-debug
BINDIR := $(BINDIR)-debug
CFLAGS += -g -O0
else
CFLAGS += -g -O2 -DNDEBUG
endif

CLEAR_VARS := $(BUILDDIR)/clear-vars.mk
BUILD_EXECUTABLE := $(BUILDDIR)/build-executable.mk
BUILD_STATIC_LIB := $(BUILDDIR)/build-static-lib.mk

d_cflags = -MMD -MP -MF $(patsubst %.o,%.d,$(1))
module-name-to-static-lib = $(1:%=lib%.a)
module-static-lib-files = $(addprefix $(OBJDIR)/,$(foreach lib,$(1),$(call module-name-to-static-lib,$(lib))))

.PHONY: all
