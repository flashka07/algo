include $(BUILDDIR)/build-steps.mk

$(eval $(call _COMPILE))

$(eval $(call _LINK_EXECUTABLE))

$(eval $(LOCAL_MODULE) : $(BINDIR)/$(LOCAL_MODULE))

$(eval $(call _CLEAN, $(BINDIR)/$(LOCAL_MODULE)))
