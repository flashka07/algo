include $(BUILDDIR)/build-steps.mk

$(eval $(call _COMPILE))

$(eval $(call _LINK_STATIC))

$(eval $(LOCAL_MODULE) : $(OBJDIR)/$(call module-name-to-static-lib,$(LOCAL_MODULE)))

$(eval $(call _CLEAN, $(OBJDIR)/$(call module-name-to-static-lib,$(LOCAL_MODULE))))
